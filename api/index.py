from flask import Flask, render_template_string
import whois

app = Flask(__name__)

@app.route('/')
def check_domain_availability():
    domain = "wpteamwork.com"
    try:
        w = whois.whois(domain)
        if w.status == None:
            return f"{domain} is available!"
        else:
            return f"{domain} is already registered."
    except Exception as e:
        return f"Error checking {domain}: {str(e)}"

if __name__ == "__main__":
    app.run(debug=True)
else:
    from werkzeug.middleware.proxy_fix import ProxyFix
    app.wsgi_app = ProxyFix(app.wsgi_app)
